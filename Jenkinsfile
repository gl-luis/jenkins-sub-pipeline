// For the sake of this example, we will use the following 
// structure with 3 slices of repositories:
def repos_slice1 = ["sw-test-1", "sw-test-2", "sw-test-3"] // We can add as many repos as we want (recommended to keep it under 10)
def repos_slice2 = ["sw-test-4", "sw-test-5", "sw-test-6"]
def repos_slice3 = ["sw-test-7", "sw-test-8", "sw-test-9"]
// You can add more slices if needed for your 70+ repositories. you will need to add more stages and parallel steps

def group = "lab4620617" // This is the group name in GitLab
def repojobs = [:]

pipeline {
  agent {
    kubernetes {
      yamlFile 'client/KubernetesPod.yaml'
    }
  }
  stages {
    stage('SSH Prep') {
      steps {
        sh 'mkdir -p ~/.ssh && echo "Host *" > ~/.ssh/config && echo " StrictHostKeyChecking no" >> ~/.ssh/config'
      }
    }
    stage('Checkout Slice1') {
      steps {
        script {
            // Add the array from above
            for (x in repos_slice1) {
              def project = x
              // We need a map here to pass it to the parallel step
              repojobs[project] = {
                dir(project)
                {
                  git branch: "main",
                  credentialsId: 'gitlab-creds',
                  url: "git@gitlab.com:${group}/${project}.git"
                }
              }
            }
            parallel repojobs
        }
      }
      post {
        success {
          script {
            sh 'echo "All repos on Slice1 have been cloned successfully"'
          }
        }
        failure {
          script {
              error "Project clones failed, stopping pipeline now..."
          }
        }
        unstable {
          script {
            error "Clonings are unstable, stopping pipeline now..."                    
          }
        }
      }
    } 
    stage('Checkout Slice2') {
      steps {
        script {
            repojobs = [:] // Reset the repojobs map
            // Add the array from above
            for (x in repos_slice2) {
              def project = x
              // We need a map here to pass it to the parallel step
              repojobs[project] = {
                dir(project)
                {
                  git branch: "main",
                  credentialsId: 'gitlab-creds',
                  url: "git@gitlab.com:${group}/${project}.git"
                }
              }
            }
            parallel repojobs
        }
      }
      post {
        success {
          script {
            sh 'echo "All repos on Slice2 have been cloned successfully"'
          }
        }
        failure {
          script {
              error "Project clones failed, stopping pipeline now..."
          }
        }
        unstable {
          script {
            error "Clonings are unstable, stopping pipeline now..."                    
          }
        }
      }
    }
    stage('Checkout Slice3') {
      steps {
        script {
            repojobs = [:] // Reset the repojobs map
            // Add the array from above
            for (x in repos_slice3) {
              def project = x
              // We need a map here to pass it to the parallel step
              repojobs[project] = {
                dir(project)
                {
                  git branch: "main",
                  credentialsId: 'gitlab-creds',
                  url: "git@gitlab.com:${group}/${project}.git"
                }
              }
            }
            parallel repojobs
        }
      }
      post {
        success {
          script {
            sh 'echo "All repos on Slice3 have been cloned successfully"'
          }
        }
        failure {
          script {
              error "Project clones failed, stopping pipeline now..."
          }
        }
        unstable {
          script {
            error "Clonings are unstable, stopping pipeline now..."                    
          }
        }
      }
    }
    stage('Git Clone finisher') {
      steps {
        container('busybox') {
          sh 'echo "*** BUILD NUMBER: ${BUILD_NUMBER}, FINISHED CLONING GIT REPOS"'
          sh 'echo "DO SOMETHING HERE"'
        }
      }
    }
  }
}